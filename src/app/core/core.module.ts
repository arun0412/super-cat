import { NgModule } from '@angular/core';
import { ErrorCatchingInterceptor } from './http-interceptors/error-interceptor/error-interceptor';
import { TokenInterceptor } from './http-interceptors/token-interceptor/token-interceptor';

@NgModule({
  declarations: [],
  imports: [],
  providers: [ErrorCatchingInterceptor, TokenInterceptor],
  bootstrap: []
})
export class CoreModule { }
