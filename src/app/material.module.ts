import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { ScrollingModule } from '@angular/cdk/scrolling';

@NgModule({
  exports: [
    MatCardModule,
    ScrollingModule
  ]
})
export class MaterialModule {}
