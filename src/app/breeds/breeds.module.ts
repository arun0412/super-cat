import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';

import { SharedModule } from './../shared/shared.module';
import { BreedsComponent } from './breeds.component';
import { BreedsListComponent } from './breeds-list/breeds-list.component';
import { BreedsService } from './services/breeds.service';

@NgModule({
  declarations: [
    BreedsComponent,
    BreedsListComponent
  ],
  imports: [
    SharedModule,
    HttpClientModule,
    CommonModule
  ],
  providers: [
    BreedsService,
  ],
  bootstrap: []
})
export class BreedModule { }
