import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BreedsComponent } from './breeds.component';

describe('BreedComponent', () => {
    let component: BreedsComponent;
    let fixture: ComponentFixture<BreedsComponent>;

    beforeEach((() => {
        TestBed.configureTestingModule({
            declarations: [BreedsComponent]
        })
            .compileComponents();

        fixture = TestBed.createComponent(BreedsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
