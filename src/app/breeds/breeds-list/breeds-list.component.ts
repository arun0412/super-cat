import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';

import { Breed } from '../../shared/shared.module';
import { BreedsService } from './../services/breeds.service';

@Component({
  selector: 'app-breeds-list',
  templateUrl: './breeds-list.component.html'
})
export class BreedsListComponent implements OnInit {
  breed$: Observable<Breed[]>;

  constructor(public breedsService: BreedsService) {
  }

  ngOnInit(): void {
    this.breed$ = this.breedsService.getBreeds();
  }
}
