import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { BreedsListComponent } from './breeds-list.component';

describe('BreedListComponent', () => {
    let component: BreedsListComponent;
    let fixture: ComponentFixture<BreedsListComponent>;

    beforeEach((() => {
        TestBed.configureTestingModule({
            declarations: [BreedsListComponent],
            imports: [HttpClientTestingModule]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BreedsListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
