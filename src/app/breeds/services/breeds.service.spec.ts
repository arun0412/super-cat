import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { BreedsService } from './breeds.service';
import { Breed } from '../../shared/shared.module';

describe('BreedService', () => {
    let httpTestingController: HttpTestingController;
    let mockBreedService: BreedsService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule]
        });

        httpTestingController = TestBed.inject(HttpTestingController);

        mockBreedService = TestBed.inject(BreedsService);
    });

    afterEach(() => {
        httpTestingController.verify();
    });

    it('#getBreeds should return expected data', (done) => {
        const expectedData: Breed[] = [
            { name: 'test1', origin: 'test1', description: 'test1', imageUrl: undefined },
            { name: 'test2', origin: 'test2', description: 'test2', imageUrl: undefined }
        ];

        mockBreedService.getBreeds().subscribe(data => {
            expect(data).toEqual(expectedData);
            done();
        });

        const testRequest = httpTestingController.expectOne('https://api.thecatapi.com/v1/breeds');

        testRequest.flush(expectedData);
    });

    it('#getBreeds should use GET to retrieve data', () => {
        mockBreedService.getBreeds().subscribe();

        const testRequest = httpTestingController.expectOne('https://api.thecatapi.com/v1/breeds');

        expect(testRequest.request.method).toEqual('GET');
    });

    it('#getBreeds should return an empty object on error', (done) => {
        const expectedData: Breed[] = [];

        mockBreedService.getBreeds().subscribe(data => {
            expect(data).toEqual(expectedData);
            done();
        });

        const testRequest = httpTestingController.expectOne('https://api.thecatapi.com/v1/breeds');

        testRequest.flush('error', { status: 500, statusText: 'Broken Service' });
    });
});
