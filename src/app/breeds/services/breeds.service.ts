import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { Breed } from '../../shared/shared.module';
import { BreedApiResponse } from '../models/breed-api-response.model';

@Injectable({
    providedIn: 'root',
})
export class BreedsService {

    private REST_API_SERVER = 'https://api.thecatapi.com/v1/breeds';

    constructor(private httpClient: HttpClient) { }

    public getBreeds(): Observable<Breed[]> {
        return this.httpClient.get(this.REST_API_SERVER)
            .pipe(map((data: BreedApiResponse[]) => data.map(this.mapBreedReponse)))
            .pipe(catchError(error => of<Breed[]>([])));
    }

    private mapBreedReponse(breedApiResponse: BreedApiResponse): Breed {
        return {
            name: breedApiResponse.name,
            description: breedApiResponse.description,
            origin: breedApiResponse.origin,
            imageUrl: breedApiResponse.image?.url
        } as Breed;
    }
}
