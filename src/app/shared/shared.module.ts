import { CardListComponent } from './components/card-list/card-list.component';
import { NgModule } from '@angular/core';
import { MaterialModule } from '../material.module';

export  { Breed } from './models/breed.model';

@NgModule({
  declarations: [
    CardListComponent
  ],
  imports: [
    MaterialModule
  ],
  exports: [
    CardListComponent
  ]
})
export class SharedModule { }
