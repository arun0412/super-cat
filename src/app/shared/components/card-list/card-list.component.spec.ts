import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';

import { CardListComponent } from './card-list.component';

describe('CardListComponent', () => {
    let component: CardListComponent;
    let fixture: ComponentFixture<CardListComponent>;

    const mockDataSource = [
        {
            imageUrl: 'https://cdn2.thecatapi.com/images/0XYvRd7oD.jpg',
            name: 'test',
            origin: 'test',
            description: 'test'
        },
        {
            imageUrl: 'https://cdn2.thecatapi.com/images/0XYvRd7oD.jpg',
            name: 'test',
            origin: 'test',
            description: 'test'
        }
    ];

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [CardListComponent],
            imports: [MatCardModule]
        }).compileComponents();

        fixture = TestBed.createComponent(CardListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should create cards with proper title, subtitle, and content', () => {

        component.dataSource = mockDataSource;

        const cardListItems = Array.from(
            document.getElementsByTagName('mat-card')
        );

        cardListItems.forEach(card => {
            const cardTitle = card.getElementsByTagName('mat-card-title')[0]
                .textContent;
            const cardSubtitle = card.getElementsByTagName('mat-card-subtitle')[0]
                .textContent;
            const cardContent = card.getElementsByTagName('mat-card-content')[0]
                .textContent;

            expect(mockDataSource).toContain(
                jasmine.objectContaining({
                    title: cardTitle,
                    subtitle: cardSubtitle,
                    content: cardContent
                })
            );
        });
    });

    it('should not create cards for the empty input', () => {

        component.dataSource = [];

        const cardListItems = document.getElementsByTagName('mat-card');

        expect(cardListItems.length).toEqual(0);
    });
});
