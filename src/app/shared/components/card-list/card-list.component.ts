import { Component, Input } from '@angular/core';

import { Breed } from '../../shared.module';

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss']
})
export class CardListComponent {
    @Input() dataSource: Breed[];
}
