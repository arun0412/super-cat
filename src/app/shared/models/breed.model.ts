export interface Breed {
    name: string;
    description: string;
    origin: string;
    imageUrl: string;
}
